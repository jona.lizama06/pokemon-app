import pokemonApi from "../api/pokemonApi"

/**
 * 
 * @returns array de numeros desde el 1 al 650.
 */
const extraerPokemones = () => {
    const pokemonArr = Array.from(Array(650))
    return pokemonArr.map((_, i) => i + 1)
}


const getPokemonOptions = async () => {
    const mezclaPokemon = extraerPokemones().sort(() => Math.random() - 0.5)
    console.log(mezclaPokemon)
    const pokemons =  await getPokemonNames(mezclaPokemon.splice(0, 4))
     
    console.table(pokemons)
    return pokemons
}

/* Se realiza una desestructuración para obtener los primeros 4 digitos del array */
const getPokemonNames = async( [a,b,c,d] = [] ) => {
    //realizamos una petición para las 4 id  de manera simultanea
    /* Creamos un arreglo de promesas*/
    const arregloPromesas = [
    pokemonApi.get(`${a}`),
    pokemonApi.get(`${b}`),
    pokemonApi.get(`${c}`),
    pokemonApi.get(`${d}`)
    ]
    const [p1,p2,p3,p4] = await Promise.all(arregloPromesas)

    return [
        {name: p1.data.name, id: p1.data.id},
        {name: p2.data.name, id: p2.data.id},
        {name: p3.data.name, id: p3.data.id},
        {name: p4.data.name, id: p4.data.id}
    ]
}




export default getPokemonOptions
